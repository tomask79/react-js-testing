import React, { useContext } from 'react';
import {MyValueContext} from './FunctionC3';

export const SetFunction = () => {
    const {value, setValue} = useContext(MyValueContext);
    return (
        <div>
            <input type="button" value="This" onClick={()=>{
                setValue(value+1);
            }} />
        </div>
    );
}