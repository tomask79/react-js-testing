import React from 'react';
import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { throwStatement } from '@babel/types';

import {FunctionC1} from './FunctionC1.js';
import {EffectFunction} from './FunctionC2';
import {MyValueContext} from './FunctionC3';
import { SetFunction } from './FunctionC4';
import {FunctionC5} from './FunctionC5';
import Comp1 from './Comp1';

function StateTest(props) {
  var object = {
    name: 'Tomas',
    surname: 'Kloucek',
    count: props.initCount
  };

  const [state, setObject] = useState(object);

  return (
    <div>
      Name of the person: { state.name } <br />
      Surname of the per: { state.surname } <br />
      Count of the perso: { state.count } < br />
      <button type="button" onClick = {
        function() {
          state.count++;
          setObject({ ...state,
                      count: state.count
                    });
        }
      }>
      Click me!</button>
    </div>
  );
}

class TestApp4 extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      count: props.initCount
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClickDecrease = this.handleClickDecrease.bind(this);
  }

  handleClick() {    
    this.setState(state=>({ count: state.count++ }));  
  }

  handleClickDecrease() {
    this.setState(function(state) {
      var v = state.count;
      v = v - 1;
      return { count: v }
    });
  }

  render() {
    return (
      <div>
        <button onClick={this.handleClick}>Increasing values!</button>
        <button onClick={this.handleClickDecrease}>Decreasing values!</button>

        This is number of clicks! {this.state.count}
      </div>
    );
  }
}

class ToggleComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(function(state) {
      return {visible: !state.visible};
    });
  }

  render() {
    let elementVariable = <p> Hello I am visible! </p>;
    if (!this.state.visible) {
      elementVariable = <p> Hello I am not visible! </p>;
    }
    return (
      <div>
        <button onClick={this.handleClick}>Switch button</button>
        {elementVariable}
      </div>
    );
  }
}

function PropsComponent(props) {
  return (
    <div>
      Hello this is the {props.test}
    </div>
  );
}

class FormComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    alert("You are submitting " + this.state.value);
  }

  handleOnChange(e) {
    e.preventDefault();
    this.setState({value: e.target.value});
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} >
          Form value: {this.state.value} <br/>
          <input type="text" value={this.state.value} onChange={this.handleOnChange}/>
          <input type="submit" value="Submit!"/>
        </form>
      </div>
    );
  }
}

function App() {
  const [value,setValue] = useState(0);
  return (
    <div className="App">
      {/*
      <FunctionC5 initState={0} /> <br />
      <MyValueContext.Provider value={{value, setValue}}>
        <EffectFunction initCount={0} />
        <SetFunction />
      </MyValueContext.Provider>
      */}
      <Comp1 />
    </div>
  );
}

export default App;
