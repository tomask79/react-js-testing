import { useReducer } from 'react';
import React from 'react';
import { ReducerAPI } from './ReducerAPI';

export const FunctionC5 = (props) => {
    const [count, dispatch] = useReducer(ReducerAPI, props.initState);
    return (
        <div>Hello I am function 5 <br />
            {count}
            <input type="button" value="add!" onClick={ () => {
                dispatch('add');
            }} /> <br />
            <input type="button" value="sub!" onClick={ () => {
                dispatch('sub');
            }} /> <br />
        </div>
    );
}