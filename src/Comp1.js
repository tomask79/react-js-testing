import React, { useState, useCallback } from 'react';

import Count from './Count.js';
import LabelComponent from './LabelComponent';
import SetCount from './SetCount';

function Comp1() {
    const [count, setCount] = useState(0);
    const [age, setAge] = useState(20);

    const incrementCount = useCallback(function incrementCount() {
        setCount(count + 1);
    }, [count]);

    const incrementAge = useCallback(function incrementAge() {
        setAge(age + 1);
    }, [age]);

    return (
        <div>
            <Count count={count} name="count" />
            <SetCount handleCount={incrementCount} name="count" />
            <LabelComponent />
            <Count count={age} name="age" />
            <SetCount handleCount={incrementAge} name="age" />
        </div>
    );
}

export default Comp1;