import React from 'react';

function LabelComponent() {
    console.log('Rendering Label component...')
    return (
        <h1>I am label component!</h1>
    );
}

export default React.memo(LabelComponent);