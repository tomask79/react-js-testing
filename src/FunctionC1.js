import React from 'react';
import { useState, useRef } from 'react';

export const FunctionC1 = () => {

    const thisRef = useRef(null);
    return (
        <div>
            Exported function! <br />

            <button type="button" 
            onClick={ ()=> { 
                thisRef.current.src
                ="https://www.top09.cz/files/photos/large/20191202143811.jpg?crop-width=264&crop-height=160"}}>
                Smiling!</button> <br />

              <button type="button" 
            onClick={ ()=> { 
                thisRef.current.src
                ="https://www.top09.cz/files/photos/large/20200527151931.jpg?crop-width=264&crop-height=160"}}>
                Whole crew!</button> <br />

            <img ref={thisRef} src="https://www.top09.cz/files/photos/large/20190724113700.jpg?crop-width=264&crop-height=160" />
        </div>
    );
}

