import React from 'react';

function Count(props) {
    console.log('Count renders '+props.name);
    return (
        <h1>{props.count}</h1>
    );
}

export default React.memo(Count);