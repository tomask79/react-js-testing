import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { MyValueContext } from './FunctionC3.js';

export const EffectFunction = (props) => {
    const [count, setCount] = useState(0);

    const {value, setValue} = useContext(MyValueContext)

    useEffect(() => {
        console.log('Yes this is '+count);
    })

    return (
        <div>
            Hello I am effect function! {count} <br />

            Hello and this is passed context {value} < br/>

            <input type="button" onClick={()=>{
                var newCount = count + 1;
                setCount(newCount);
                }
            } value="Click me!" />
        </div>
    );
}