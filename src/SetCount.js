import React from 'react';

function SetCount({ handleCount, name }) {
    console.log('Rendering setCount '+name);
    return (
        <input type="button" onClick={handleCount} value="SetCount" />
    );
}

export default React.memo(SetCount);